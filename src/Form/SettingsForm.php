<?php

namespace Drupal\social_content_locker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures aggregator settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_content_locker_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_content_locker.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_content_locker.settings');

    $form['basic_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Basic settings'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['basic_settings']['skin'] = [
      '#type' => 'select',
      '#title' => $this->t('Social Content locker style'),
      '#options' => ['light' => 'Light', 'dark' => 'Dark'],
      '#default_value' => $config->get('basic.skin'),
      '#empty_value' => 0,
    ];

    $form['basic_settings']['ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load content via ajax'),
      '#default_value' => $config->get('basic.ajax'),
      '#description' => $this->t('If you choose "Use ajax" option, it\'s mean we remove hidden fields from render and return them via ajax.'),
    ];

    $form['basic_settings']['cookie'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cookie'),
      '#default_value' => $config->get('basic.cookie'),
    ];

    $form['basic_settings']['cookie_lifetime'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie lifetime'),
      '#default_value' => $config->get('basic.cookie_lifetime'),
      '#states' => [
        'visible' => [
          ':input[name="basic_settings[cookie]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('social_content_locker.settings')
      ->set('basic', $form_state->getValue('basic_settings'))->save();

    parent::submitForm($form, $form_state);
  }

}
